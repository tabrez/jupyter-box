# Set up workspace for fast.ai coursework

## Quickstart

```sh
multipass launch docker -n jupyter-box --cloud-init cloud-config.yaml -c 8 -m 5G && mpc info jupyter-box
multipass mount $PWD jupyter-box:/home/ubuntu/work && mpc exec jupyter-box -- docker restart jupyterlab
multipass exec jupyter-box -- docker logs jupyterlab
```

Click on the URL shown by `docker logs` and replace the ip address in the url by the IP address of the VM shown by `multipass info jupyter-box` command

## Pull the image from Gitlab and run it

```sh
docker compose up -d && docker compose logs -f
# or
docker run -d --rm \
  --runtime=nvidia \
  --gpus all \
  -p 8888:8888 \
  --restart=unless-stopped
  -v /home/tabrez/code:/home/jovyan/work \
  --ipc=host  \
  registry.gitlab.com/tabrez/dkr-images/jupyter/fastai:0.4
docker logs -f jupyterlab
```

Press Ctrl-C after you click on the URL to access jupyter lab

## Build new jupyter docker image and run it

Modify Dockerfile if needed and then run the following commands:

```sh
docker build -t jupyter/fastai:0.1 .
docker run --runtime=nvidia --gpus all -p 8888:8888 \
  -v /home/tabrez/code:/home/jovyan/work \
  --ipc=host  \
  jupyter/fastai:0.1
```

## Build image and push to registry

```sh
docker build -t registry.gitlab.com/tabrez/dkr-images/jupyter/fastai:0.1 .
docker login registry.gitlab.com
docker push registry.com/tabrez/dkr-images/jupyter/fastai:0.1
```

## Create a new container with fastbook, pytorch packages installed

When we run `docker run`, packages get installed from `setup.sh` script. We can use 
`docker commit` to save the state so we don't have to go through the long installation
process on every container restart.

```sh
docker run --runtime=nvidia --gpus all -p 8888:8888 \
  -v /home/tabrez/code:/home/jovyan/work \
  --ipc=host  \
  registry.gitlab.com/tabrez/dkr-images/jupyter/fastai:0.1
docker commit \
  -m 'setup.sh was run on container start, cuda working' \
  -a "Tabrez" \
  jupyterlab registry.gitlab.com/tabrez/dkr-images/jupyter/fastai-big:0.1
docker push registry.gitlab.com/tabrez/dkr-images/jupyter/fastai-big:0.1
```

## NVidia CUDA support

+ Install Nvidia GPU drivers

[Help](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html)

```sh
lspci | egrep -i 'vga|3d|display'
lsmod | grep nvidia
uname -mr && cat /etc/*release
ubuntu-drivers devices
sudo apt install -y nvidia-driver-535
nvidia-smi
```

## Install Docker

[Help](https://docs.docker.com/engine/install/ubuntu/)

```sh
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh ./get-docker.sh --dry-run
```

## Install NVidia Container Toolkit

[Help](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html)

```sh
$ distribution=$(. /etc/os-release;echo $ID$VERSION_ID) \
      && curl -fsSL https://nvidia.github.io/libnvidia-container/gpgkey | sudo gpg --dearmor -o /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg \
      && curl -s -L https://nvidia.github.io/libnvidia-container/$distribution/libnvidia-container.list | \
            sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://#g' | \
            sudo tee /etc/apt/sources.list.d/nvidia-container-toolkit.list

sudo apt-get update
sudo apt-get install -y nvidia-container-toolkit
sudo nvidia-ctk runtime configure --runtime=docker
sudo systemctl restart docker
```

## Test if CUDA is working from inside docker container

```sh
sudo docker run --rm --runtime=nvidia --gpus all nvidia/cuda:12.2.0-runtime-ubuntu22.04 nvidia-smi
# or:
docker compose -f docker-compose-nvidia-cuda.yml up test
```

## Fix if ubuntu desktop is bricked

Install the headers and extra modules for newly installed kernel if they're missing:

```sh
sudo apt install linux-headers-$(uname -r)
sudo apt-get install linux-modules-extra-$(uname -r)
```

NVidia might have changed the OS to use nvidia's version of latest kernel, change it
back to stable kernel version for your OS.

+ Boot into recovery mode, press ctrl+alt+F1/F2/1/2 to get alternative console if needed
+ Install kernel 5.19 and re-install nvidia drivers/container toolkit

```sh
sudo apt purge nvidia*
sudo apt autoremove
sudo apt update -y && sudo apt upgrade -y && sudo apt dist-upgrade -y && sudo apt install -f && sudo apt autoremove
# sudo apt install -y --reinstall ubuntu-gnome-desktop
ubuntu-drivers devices
sudo apt install -y nvidia-driver-535
sudo apt-get install -y nvidia-container-toolkit
```

+ Run the newly installed kernel from advanced options in the grub menu
+ Re-install nvidia drivers from the command line
+ Update grub menu entry to boot into stable kernel by default

```sh
sudo grub-mkconfig | grep -iE "menuentry 'Ubuntu, with Linux" | awk '{print i++ " : "$1, $2, $3, $4, $5, $6, $7}'
sudo vim /etc/default/grub
sudo update-grub
```

## Run JupyterLab from our custom docker image

You can skip the steps to install python packages in the following sections if you
build and run our custom docker image

```sh
docker build -t tabrez/pytorch-notebook .
docker run -it \
  --runtime=nvidia \
  --gpus all \
  -p 8888:8888 \
  -v "${PWD}":/home/jovyan/work \
  --user "$(id -u)" \
  --group-add users \
  --ipc=host  tabrez/pytorch-notebook
```

## Run JupyterLab from official jupyter docker image

[Help](https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html)
[DockerHub page](https://hub.docker.com/r/jupyter/base-notebook/tags/)

```sh
docker run --runtime=nvidia --gpus all -p 8888:8888 \
  -v "${PWD}":/home/jovyan/work \
  --user "$(id -u)" --group-add users \
  jupyter/base-notebook:ubuntu-22.04
```

## Install numpy and pytorch packages inside docker container and test if notebooks are working

[Help](https://pytorch.org/get-started/locally/)

```sh
mamba install --yes numpy
mamba install --yes pytorch torchvision torchaudio pytorch-cuda=11.7 -c pytorch -c nvidia
```

```py
import numpy as np
np.arange(15).reshape(3,5)
```

[Help](https://wandb.ai/ayush-thakur/dl-question-bank/reports/How-To-Check-If-PyTorch-Is-Using-The-GPU--VmlldzoyMDQ0NTU)

```py
import torch
print(f'random value: {torch.rand(5, 3)}')
print(f'pytorch cuda version: {torch.version.cuda}')
torch.cuda.current_device(), torch.cuda.device_count()
```

## Install fastbook and test if model is training fast enough

```sh
mamba install --yes sentenceperiod ipywidgets
mamba install --yes fastbook -c fastchan
```

```py
import fastbook
fastbook.setup_book()

from fastbook import *

from fastai.vision.all import *
path = untar_data(URLs.PETS)/'images'

def is_cat(x): return x[0].isupper()
dls = ImageDataLoaders.from_name_func(
    path, get_image_files(path), valid_pct=0.2, seed=42,
    label_func=is_cat, item_tfms=Resize(224))

learn = vision_learner(dls, resnet34, metrics=error_rate)
learn.fine_tune(1)
```

