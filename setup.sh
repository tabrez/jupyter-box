#!/bin/bash

mamba install --yes -c fastchan -c pytorch -c nvidia \
  fastbook=0.0.28 \
  pytorch=py3.9_cuda11.8_cudnn8.7.0_0 \
  torchvision=py39_cu118 \
  torchaudio=py39_cu118 \
  pytorch-cuda=11.7 \
  && mamba clean --all -f -y \

fix-permissions "/home/${NB_USER}"
fix-permissions "${CONDA_DIR}"
# exec start-notebook.sh

