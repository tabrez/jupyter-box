ARG OWNER=tabrez
# Following is the digest of `ubuntu-22.04` image as of 3 Aug, 2023
# https://hub.docker.com/layers/jupyter/base-notebook/hub-4.0.1/images/sha256-ce897216428179abc53588234419d206351e5d8a64e9605fe30496af980a3e1b
ARG BASE_CONTAINER=jupyter/base-notebook@sha256:ce897216428179abc53588234419d206351e5d8a64e9605fe30496af980a3e1b
FROM $BASE_CONTAINER

LABEL maintainer="Tabrez Iqbal <tabrez@mailbox.org>"

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

USER root

RUN apt-get update --yes \
  && apt-get install --yes --no-install-recommends \
  build-essential=12.9ubuntu3 \
  git=1:2.34.1-1ubuntu1.9 \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

RUN echo "${NB_USER} ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/${NB_USER}

USER ${NB_UID}

RUN mamba install --yes -c conda-forge \
  # ipywidgets=8.1.0 \
  # sentencepiece=0.1.99 \
  # nbdev=2.2.10 \
  jupyterlab-autosave-on-focus-change=0.2.0 \
  # jupyterlab_execute_time=3.0.1 \
  nbdime=3.2.1 \
  && mamba clean --all -f -y \
  && jupyter labextension disable "@jupyterlab/apputils-extension:announcements" \
  && nbdime config-git --enable --global \
  && fix-permissions "/home/${NB_USER}" \
  && fix-permissions "${CONDA_DIR}"

COPY --chown=${NB_USER} overrides.json /opt/conda/share/jupyter/lab/settings/overrides.json

COPY --chown=${NB_USER} setup.sh /setup.sh
RUN bash /setup.sh
# CMD ["bash", "/setup.sh"]
CMD ["exec", start-notebook.sh"]

